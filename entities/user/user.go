package user

import "strings"

type Gender int

const (
	Any    Gender = 0
	Male   Gender = 1
	Female Gender = 2
)

type User struct {
	Id     int64
	Name   string
	Login  string
	Karma  int
	Gender Gender
}

func (a Gender) Compatible(b Gender) bool {
	return a == Any || b == Any || a == b
}

func (self Gender) String() string {
	switch self {
	case Any:
		return "any"
	case Male:
		return "male"
	case Female:
		return "female"
	}
	panic("unknown gender")
}

func StringToGender(src string) (Gender, bool) {
	switch strings.ToLower(src) {
	case "male":
		return Male, true
	case "m":
		return Male, true
	case "female":
		return Female, true
	case "f":
		return Female, true
	case "any":
		return Any, true
	}
	return Any, false
}

func (self *User) GetName() string {
	if self.Login != "" {
		return self.Login
	}
	return self.Name
}
