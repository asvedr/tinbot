package errs

type UserNotFound struct{}

func (UserNotFound) Error() string {
	return "UserNotFound"
}
