package karma

const (
	Min int = -10
	Max int = 10
)

func Normalize(val int) int {
	return max(min(val, Max), Min)
}
