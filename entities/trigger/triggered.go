package trigger

import (
	"regexp"
	"tinbot/entities/user"
)

type Request struct {
	SubjUser       user.User
	MentionedUsers []user.User
	Text           string
}

type Schema struct {
	Reaction string
	ReName   string
	Re       *regexp.Regexp
	Prob     float64
}

type Triggered struct {
	Key       string
	Vars      map[string]string
	Mentioned *user.User
	Subj      *user.User
	KarmaDiff int
}
