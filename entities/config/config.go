package config

type Config struct {
	Token          string   `default:""`
	NameSchemaPath string   `default:"./configs/users.json"`
	ReactionsPath  string   `default:"./configs/reactions.json"`
	RegexPath      string   `default:"./configs/regex.json"`
	TriggersPath   string   `default:"./configs/triggers.json"`
	UserDbPath     string   `default:"./configs/user_db.json"`
	Meta           struct{} `prefix:"tinbot_"`
}
