package regex_parser

import (
	"encoding/json"
	"fmt"
	"io"
	"regexp"
	"tinbot/proto"
)

type parser struct{}

func New() proto.IRegexParser {
	return parser{}
}

func (parser) Parse(src io.Reader) (map[string]*regexp.Regexp, error) {
	decoder := json.NewDecoder(src)
	decoder.DisallowUnknownFields()
	buffer := map[string]string{}
	err := decoder.Decode(&buffer)
	if err != nil {
		return nil, err
	}
	result := map[string]*regexp.Regexp{}
	for key, val := range buffer {
		re, err := regexp.Compile(val)
		if err != nil {
			err = fmt.Errorf("can not compile re [[ %s ]]", val)
			return nil, err
		}
		result[key] = re
	}
	return result, nil
}
