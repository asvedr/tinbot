package reactor_test

import (
	"testing"
	"tinbot/entities/user"
	"tinbot/impls/reactor"

	"github.com/stretchr/testify/assert"
)

func TestNoReplacement(t *testing.T) {
	r := reactor.New("ho ho ho")
	assert.Equal(t, r.React(nil, nil, nil), "ho ho ho")
}

func TestVars(t *testing.T) {
	r := reactor.New("ho $1 $2")
	vars := map[string]string{"1": "x"}
	assert.Equal(t, r.React(nil, nil, vars), "ho x $2")
	vars["2"] = "y"
	assert.Equal(t, r.React(nil, nil, vars), "ho x y")
}

func TestSubj(t *testing.T) {
	r := reactor.New("hi @subj")
	assert.Equal(t, r.React(nil, nil, nil), "hi @subj")
	subj := &user.User{Name: "X", Login: "@y"}
	assert.Equal(t, r.React(subj, nil, nil), "hi @y")
	subj.Login = ""
	assert.Equal(t, r.React(subj, nil, nil), "hi X")
}

func TestMent(t *testing.T) {
	r := reactor.New("hi @ment")
	assert.Equal(t, r.React(nil, nil, nil), "hi @ment")
	ment := &user.User{Name: "X", Login: "@y"}
	assert.Equal(t, r.React(nil, ment, nil), "hi @y")
	ment.Login = ""
	assert.Equal(t, r.React(nil, ment, nil), "hi X")
}
