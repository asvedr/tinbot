package reactor

import (
	"strings"
	"tinbot/entities/consts"
	"tinbot/entities/user"
	"tinbot/proto"
)

type reactor struct {
	template string
}

func New(template string) proto.IReactor {
	return &reactor{template: template}
}

func (self *reactor) React(
	subj *user.User,
	ment *user.User,
	vars map[string]string,
) string {
	msg := self.template
	if subj != nil {
		msg = strings.ReplaceAll(msg, consts.SubjUser, subj.GetName())
	}
	if ment != nil {
		msg = strings.ReplaceAll(msg, consts.MentionedUser, ment.GetName())
	}
	for var_name, var_val := range vars {
		msg = strings.ReplaceAll(msg, "$"+var_name, var_val)
	}
	return msg
}
