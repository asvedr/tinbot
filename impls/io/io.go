package io

import (
	"errors"
	"os"
	"tinbot/proto"
)

type io struct{}

func New() proto.IIO { return io{} }

func (io) ReadFile(path string) ([]byte, error) {
	return os.ReadFile(path)
}

func (io) WriteFile(path string, data []byte) error {
	return os.WriteFile(path, data, 0644)
}

func (io) CheckFileExists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}
