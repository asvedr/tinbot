package state_maker

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"regexp"
	"tinbot/entities/config"
	"tinbot/proto"
)

type maker struct {
	io       proto.IIO
	reaction proto.IReactionParser
	regex    proto.IRegexParser
	trigger  proto.ITriggerParser
	names    proto.IUserConfigParser
}

func New(
	io proto.IIO,
	reaction proto.IReactionParser,
	regex proto.IRegexParser,
	trigger proto.ITriggerParser,
	names proto.IUserConfigParser,
) proto.IStateMaker {
	return &maker{
		io:       io,
		reaction: reaction,
		regex:    regex,
		trigger:  trigger,
		names:    names,
	}
}

func (self *maker) Make(cnf *config.Config) (proto.State, error) {
	var res proto.State
	re := map[string]*regexp.Regexp{}
	p_trigs := func(r io.Reader) ([]proto.ITrigger, error) {
		return self.trigger.Parse(re, r)
	}
	io := self.io
	err := errors.Join(
		read(io, cnf.RegexPath, self.regex.Parse, &re),
		read(io, cnf.ReactionsPath, self.reaction.Parse, &res.Reactions),
		read(io, cnf.TriggersPath, p_trigs, &res.Triggers),
		read(io, cnf.NameSchemaPath, self.names.Parse, &res.UserConfig),
	)
	return res, err
}

func read[T any](
	io proto.IIO,
	path string,
	f func(r io.Reader) (T, error),
	dst *T,
) error {
	bts, err := io.ReadFile(path)
	if err != nil {
		return err
	}
	val, err := f(bytes.NewBuffer(bts))
	if err != nil {
		return fmt.Errorf("can not process %s: %v", path, err)
	}
	*dst = val
	return nil
}
