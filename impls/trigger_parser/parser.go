package trigger_parser

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"regexp"
	"tinbot/entities/user"
	"tinbot/impls/trigger"
	"tinbot/proto"
)

type parser struct{}

type item struct {
	Key       string   `json:"key"`
	Prob      *float64 `json:"prob"`
	Re        string   `json:"re"`
	Subj      *pred    `json:"subj"`
	MentN     *pred    `json:"mentN"`
	Ment1     *pred    `json:"ment1"`
	KarmaDiff int      `json:"karma_diff"`
}

type pred struct {
	KarmaGt *int    `json:"karma_gt"`
	KarmaLt *int    `json:"karma_lt"`
	Len     *int    `json:"len"`
	LenGt   *int    `json:"len_gt"`
	LenLt   *int    `json:"len_lt"`
	Gender  *string `json:"gender"`
}

func New() proto.ITriggerParser {
	return parser{}
}

func (parser) Parse(
	re map[string]*regexp.Regexp,
	src io.Reader,
) ([]proto.ITrigger, error) {
	decoder := json.NewDecoder(src)
	decoder.DisallowUnknownFields()
	buffer := []item{}
	err := decoder.Decode(&buffer)
	if err != nil {
		return nil, err
	}
	var result []proto.ITrigger
	for _, trigger_src := range buffer {
		parsed, err := build(re, trigger_src)
		if err != nil {
			tr, _ := json.Marshal(trigger_src)
			err = fmt.Errorf("can not process trigger: %s\nerr: %v", string(tr), err)
			return nil, err
		}
		result = append(result, parsed)
	}
	return result, nil
}

func build(re_map map[string]*regexp.Regexp, item item) (proto.ITrigger, error) {
	if item.Key == "" {
		return nil, errors.New("item with empty key")
	}
	re_key := item.Re
	if re_key == "" {
		re_key = item.Key
	}
	re := re_map[re_key]
	if re == nil {
		return nil, errors.New("re '" + re_key + "' not found")
	}
	if item.Prob == nil {
		return nil, errors.New("item '" + item.Key + "' has empty prob")
	}
	subj, err := build_single_pred(item.Subj)
	if err != nil {
		return nil, errors.New("item '" + item.Key + "' invalid subj: " + err.Error())
	}
	ment1, err := build_single_pred(item.Ment1)
	if err != nil {
		return nil, errors.New("item '" + item.Key + "' invalid ment1: " + err.Error())
	}
	return &trigger.Trigger{
		Key:         item.Key,
		ReName:      re_key,
		Re:          re,
		Probability: *item.Prob,
		Subj:        subj,
		MentionedN:  build_multi_pred(item.MentN),
		Mentioned1:  ment1,
		KarmaDiff:   item.KarmaDiff,
		// Extract     func(*trigger.Request) map[string]string
	}, nil
}

func build_multi_pred(src *pred) func([]user.User) bool {
	if src == nil {
		return nil
	}
	return func(u []user.User) bool {
		l := len(u)
		if src.Len != nil {
			return l == *src.Len
		}
		if src.LenGt != nil {
			return l > *src.LenGt
		}
		if src.LenLt != nil {
			return l < *src.LenLt
		}
		return true
	}
}

func build_single_pred(src *pred) (func(user.User) bool, error) {
	if src == nil {
		return nil, nil
	}
	gender := user.Any
	converted := false
	if src.Gender != nil {
		gender, converted = user.StringToGender(*src.Gender)
		if !converted {
			return nil, errors.New("invalid gender: " + *src.Gender)
		}
	}
	return func(u user.User) bool {
		if src.KarmaGt != nil {
			if u.Karma <= *src.KarmaGt {
				return false
			}
		}
		if src.KarmaLt != nil {
			if u.Karma >= *src.KarmaLt {
				return false
			}
		}
		return gender.Compatible(u.Gender)
	}, nil
}
