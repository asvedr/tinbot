package reaction_parser

import (
	"encoding/json"
	"io"
	"tinbot/proto"
)

type parser struct {
	factory func(string) proto.IReactor
}

func New(
	factory func(string) proto.IReactor,
) proto.IReactionParser {
	return parser{factory: factory}
}

func (self parser) Parse(src io.Reader) (map[string]proto.IReactor, error) {
	dst := map[string]string{}
	decoder := json.NewDecoder(src)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&dst)
	if err != nil {
		return nil, err
	}
	res := map[string]proto.IReactor{}
	for key, val := range dst {
		res[key] = self.factory(val)
	}
	return res, nil
}
