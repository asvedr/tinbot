package engine

import (
	"log"
	"math/rand"
	"tinbot/entities/karma"
	"tinbot/entities/trigger"
	"tinbot/entities/user"
	"tinbot/proto"
)

type engine struct {
	user_repo      proto.IUserRepo
	name_extractor proto.IUserConfig
	triggers       []proto.ITrigger
	reactors       map[string]proto.IReactor
}

func New(
	user_repo proto.IUserRepo,
	name_extractor proto.IUserConfig,
	triggers []proto.ITrigger,
	reactors map[string]proto.IReactor,
) proto.IEngine {
	return &engine{
		user_repo:      user_repo,
		name_extractor: name_extractor,
		triggers:       triggers,
		reactors:       reactors,
	}
}

func (self *engine) MultProb(mult float64) {
	for _, trigger := range self.triggers {
		trigger.MultProb(mult)
	}
}

func (self *engine) React(usr user.User, msg string) (string, error) {
	mentioned, err := self.extract_mentioned(msg)
	if err != nil {
		return "", err
	}
	request := &trigger.Request{
		SubjUser:       usr,
		MentionedUsers: mentioned,
		Text:           msg,
	}
	for _, trigger := range self.shuffle_triggers() {
		triggered := trigger.Match(request)
		if triggered != nil {
			triggered.Subj = &usr
			return self.process_triggered(triggered)
		}
	}
	return "", nil
}

func (self *engine) shuffle_triggers() []proto.ITrigger {
	res := make([]proto.ITrigger, len(self.triggers))
	copy(res, self.triggers)
	rand.Shuffle(len(res), func(i, j int) {
		res[i], res[j] = res[j], res[i]
	})
	return res
}

func (self *engine) extract_mentioned(msg string) ([]user.User, error) {
	names := self.name_extractor.ExtractNames(msg)
	if len(names) == 0 {
		return []user.User{}, nil
	}
	set, err := self.user_repo.GetByLogin(names)
	if err != nil {
		return nil, err
	}
	extend, err := self.user_repo.GetByName(names)
	if err != nil {
		return nil, err
	}
	for id, usr := range extend {
		set[id] = usr
	}
	var result []user.User
	for _, usr := range set {
		result = append(result, usr)
	}
	return result, nil
}

func (self *engine) process_triggered(tr *trigger.Triggered) (string, error) {
	var err error
	if tr.KarmaDiff != 0 {
		new_karma := tr.Subj.Karma + tr.KarmaDiff
		tr.Subj.Karma = karma.Normalize(new_karma)
		err = self.user_repo.SetUsers(*tr.Subj)
	}
	if err != nil {
		return "", err
	}
	reactor := self.reactors[tr.Key]
	if reactor == nil {
		log.Print("invalid trigger key: " + tr.Key)
		return "", nil
	}
	resp := reactor.React(tr.Subj, tr.Mentioned, tr.Vars)
	return resp, nil
}
