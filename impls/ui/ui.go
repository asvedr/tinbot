package ui

import (
	"errors"
	"log"
	"strings"
	"time"
	"tinbot/entities/consts"
	"tinbot/entities/errs"
	"tinbot/entities/user"
	"tinbot/proto"

	"github.com/asvedr/gotgbot/v2"
	"github.com/asvedr/gotgbot/v2/ext"
	"github.com/asvedr/gotgbot/v2/ext/handlers"
	"github.com/asvedr/gotgbot/v2/ext/handlers/filters/message"
)

type ui struct {
	bot        *gotgbot.Bot
	engine     proto.IEngine
	users_repo proto.IUserRepo
}

func New(
	bot *gotgbot.Bot,
	engine proto.IEngine,
	users_repo proto.IUserRepo,
) proto.IUI {
	return &ui{
		bot:        bot,
		engine:     engine,
		users_repo: users_repo,
	}
}

func (self *ui) Run() {
	for {
		err := self.bot.Reconnect(nil)
		if err == nil {
			err = self.main_loop()
		}
		log.Printf("TgUi error: %v", err)
		time.Sleep(time.Second)
	}
}

func (self *ui) main_loop() error {
	bot := self.bot
	dispatcher := ext.NewDispatcher(&ext.DispatcherOpts{
		// If an error is returned by a handler, log it and continue going.
		Error: func(b *gotgbot.Bot, ctx *ext.Context, err error) ext.DispatcherAction {
			log.Println("an error occurred while handling update:", err.Error())
			return ext.DispatcherActionNoop
		},
		MaxRoutines: ext.DefaultMaxRoutines,
	})
	updater := ext.NewUpdater(dispatcher, nil)

	handler := func(bot *gotgbot.Bot, ctx *ext.Context) error {
		return self.react(ctx)
	}

	// Add echo handler to reply to all text messages.
	dispatcher.AddHandler(
		handlers.NewMessage(message.Text, handler),
	)
	err := updater.StartPolling(bot, &ext.PollingOpts{
		DropPendingUpdates: false,
		GetUpdatesOpts: &gotgbot.GetUpdatesOpts{
			Timeout: 9,
			RequestOpts: &gotgbot.RequestOpts{
				Timeout: time.Second * 10,
			},
		},
	})
	if err != nil {
		log.Printf("failed to start polling: %v", err.Error())
		return err
	}
	log.Printf("%s has been started...\n", bot.User.Username)

	// Idle, to keep updates coming in, and avoid bot stopping.
	updater.Idle()
	return errors.New("stop idling")
}

func (self *ui) react(ctx *ext.Context) error {
	tg_user := ctx.EffectiveUser
	login := tg_user.Username
	usr, err := self.update_user(
		tg_user.Id,
		"@"+strings.ToLower(strings.TrimSpace(login)),
		strings.ToLower(strings.TrimSpace(tg_user.FirstName+tg_user.LastName)),
	)
	if err != nil {
		return err
	}
	msg := ctx.EffectiveMessage.Text
	msg = strings.ToLower(strings.TrimSpace(msg))
	if has_media(ctx.EffectiveMessage) {
		msg = consts.PicValue + " " + msg
	}
	txt, err := self.engine.React(usr, msg)
	if err != nil {
		return err
	}
	if txt != "" {
		log.Printf("[replied] %s >>> %s", msg, txt)
		_, err = ctx.EffectiveMessage.Reply(self.bot, txt, nil)
	} else {
		log.Printf("[ignored] %s", msg)
	}
	return err
}

func has_media(msg *gotgbot.Message) bool {
	flags := []bool{
		msg.Audio != nil,
		msg.Document != nil,
		msg.Photo != nil,
		msg.Video != nil,
		msg.Voice != nil,
	}
	for _, flag := range flags {
		if flag {
			return true
		}
	}
	return false
}

func (self *ui) update_user(
	id int64,
	login string,
	name string,
) (user.User, error) {
	usr, err := self.users_repo.GetById(id)
	if err != nil && !errors.Is(err, errs.UserNotFound{}) {
		return usr, err
	}
	usr.Id = id
	usr.Login = login
	usr.Name = name
	err = self.users_repo.SetUsers(usr)
	if err != nil {
		return usr, err
	}
	return self.users_repo.GetById(id)
}
