package user_config

import (
	"encoding/json"
	"errors"
	"io"
	"regexp"
	"sort"
	"strings"
	"tinbot/entities/user"
	"tinbot/proto"
	"tinbot/restr"
)

type config struct {
	users         map[string]schema
	name_to_login map[string]string
	login_re      *regexp.Regexp
}

type parser struct{}

type Item struct {
	Gender string   `json:"gender"`
	Names  []string `json:"names"`
}

type schema struct {
	gender user.Gender
	name   string
	re     *regexp.Regexp
}

func NewConfig(src map[string]Item) (proto.IUserConfig, error) {
	users, ntl, err := parse_config(src)
	if err != nil {
		return nil, err
	}
	return config{
		users:         users,
		name_to_login: ntl,
		login_re:      login_re(),
	}, nil
}

func NewParser() proto.IUserConfigParser {
	return parser{}
}

func (parser) Parse(src io.Reader) (proto.IUserConfig, error) {
	decoder := json.NewDecoder(src)
	decoder.DisallowUnknownFields()
	buffer := map[string]Item{}
	err := decoder.Decode(&buffer)
	if err != nil {
		return nil, errors.New("can not decode name schema: " + err.Error())
	}
	users, ntl, err := parse_config(buffer)
	return config{
		users:         users,
		name_to_login: ntl,
		login_re:      login_re(),
	}, err
}

func (self config) GetLogin(name string) string {
	return self.name_to_login[name]
}

func (self config) GetName(login string) string {
	u, found := self.users[login]
	if !found {
		return ""
	}
	return u.name
}

func (self config) GetGender(login string) user.Gender {
	u, found := self.users[login]
	if !found {
		return user.Any
	}
	return u.gender
}

func (self config) ExtractNames(text string) []string {
	as_set := map[string]bool{}
	for _, login := range self.login_re.FindAllString(text, -1) {
		as_set[login] = true
	}
	for _, usr := range self.users {
		if usr.re.MatchString(text) {
			as_set[usr.name] = true
		}
	}
	result := []string{}
	for key := range as_set {
		result = append(result, key)
	}
	ss := sort.StringSlice(result)
	ss.Sort()
	return ss
}

func parse_config(
	src map[string]Item,
) (map[string]schema, map[string]string, error) {
	result := map[string]schema{}
	ntl := map[string]string{}
	var err error
	for key, val := range src {
		key = strings.ToLower(key)
		gender, valid := user.StringToGender(val.Gender)
		if !valid {
			return nil, nil, errors.New("invalid gender: " + val.Gender)
		}
		re, err := regexp.Compile(restr.JoinOr(val.Names...))
		if err != nil {
			return nil, nil, err
		}
		name := val.Names[0]
		sch := schema{gender: gender, name: name, re: re}
		ntl[name] = key
		result[key] = sch
	}
	return result, ntl, err
}

func login_re() *regexp.Regexp {
	return regexp.MustCompile("@[a-z]+")
}
