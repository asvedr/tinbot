package user_config_test

import (
	"testing"
	"tinbot/entities/user"
	"tinbot/impls/user_config"
	"tinbot/proto"

	"github.com/stretchr/testify/assert"
)

func new_conf() proto.IUserConfig {
	ex, err := user_config.NewConfig(
		map[string]user_config.Item{
			"@alex": {
				Gender: "f",
				Names:  []string{"саша", "саня"},
			},
		},
	)
	if err != nil {
		panic(err.Error())
	}
	return ex
}

func TestNotFound(t *testing.T) {
	ex := new_conf()
	assert.Equal(t, ex.ExtractNames(""), []string{})
	assert.Equal(t, ex.ExtractNames("abc def"), []string{})
}

func TestFoundName(t *testing.T) {
	ex := new_conf()
	assert.Equal(
		t,
		ex.ExtractNames("привет саня, как ты?"),
		[]string{"саша"},
	)
}

func TestFoundLogin(t *testing.T) {
	ex := new_conf()
	assert.Equal(
		t,
		ex.ExtractNames("привет @beb, как ты?"),
		[]string{"@beb"},
	)

}

func TestFoundMany(t *testing.T) {
	ex, _ := user_config.NewConfig(
		map[string]user_config.Item{
			"@alex": {
				Gender: "f",
				Names:  []string{"саша", "саня"},
			},
			"@tanya": {
				Gender: "f",
				Names:  []string{"таня", "танос"},
			},
		},
	)
	assert.Equal(
		t,
		ex.ExtractNames("привет саша, саня, а так же таня"),
		[]string{"саша", "таня"},
	)
}

func Test(t *testing.T) {
	ex := new_conf()
	assert.Equal(
		t,
		ex.ExtractNames("что думаешь о @user"),
		[]string{"@user"},
	)
}

func TestGetLogin(t *testing.T) {
	ex := new_conf()
	assert.Equal(t, ex.GetLogin("саша"), "@alex")
}

func TestGetName(t *testing.T) {
	ex := new_conf()
	assert.Equal(t, ex.GetName("@alex"), "саша")
}

func TestGetGender(t *testing.T) {
	ex := new_conf()
	assert.Equal(t, ex.GetGender("@alex"), user.Female)
}
