package trigger

import (
	"math/rand"
	"regexp"
	"tinbot/entities/trigger"
	"tinbot/entities/user"
	"tinbot/proto"
)

type Trigger struct {
	Key         string
	ReName      string
	Re          *regexp.Regexp
	Probability float64
	Subj        func(user.User) bool
	MentionedN  func([]user.User) bool
	Mentioned1  func(user.User) bool
	KarmaDiff   int
	// Extract     func(*trigger.Request) map[string]string
}

func (self *Trigger) Copy() proto.ITrigger {
	clone := *self
	return &clone
}

func (self *Trigger) Schema() trigger.Schema {
	return trigger.Schema{
		Reaction: self.Key,
		ReName:   self.ReName,
		Re:       self.Re,
		Prob:     self.Probability,
	}
}

func (self *Trigger) MultProb(mult float64) {
	self.Probability *= mult
}

func (self *Trigger) Match(req *trigger.Request) *trigger.Triggered {
	res := &trigger.Triggered{Key: self.Key, KarmaDiff: self.KarmaDiff}
	if !self.match_subj(req, res) || !self.match_re(req) || !self.match_mentioned(req, res) {
		return nil
	}
	if !self.check_prob() {
		return nil
	}
	return res
}

func (self *Trigger) check_prob() bool {
	return rand.Float64() <= self.Probability
}

func (self *Trigger) match_subj(
	req *trigger.Request,
	res *trigger.Triggered,
) bool {
	res.Subj = &req.SubjUser
	if self.Subj == nil {
		return true
	}
	return self.Subj(req.SubjUser)
}

func (self *Trigger) match_re(req *trigger.Request) bool {
	if self.Re == nil {
		return true
	}
	return self.Re.MatchString(req.Text)
}

func (self *Trigger) match_mentioned(
	req *trigger.Request,
	res *trigger.Triggered,
) bool {
	usrs := req.MentionedUsers
	return self.match_mentioned_n(usrs) && self.match_mentioned_1(usrs, res)
}

func (self *Trigger) match_mentioned_n(usrs []user.User) bool {
	if self.MentionedN == nil {
		return true
	}
	return self.MentionedN(usrs)
}

func (self *Trigger) match_mentioned_1(usrs []user.User, res *trigger.Triggered) bool {
	if self.Mentioned1 == nil {
		return true
	}
	for _, u := range usrs {
		if self.Mentioned1(u) {
			res.Mentioned = &u
			return true
		}
	}
	return false
}
