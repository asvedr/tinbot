package trigger_test

import (
	"regexp"
	"testing"
	ents "tinbot/entities/trigger"
	"tinbot/entities/user"
	"tinbot/impls/trigger"

	"github.com/stretchr/testify/assert"
)

func TestMatchSubj(t *testing.T) {
	trig := trigger.Trigger{
		Key:         "key",
		Probability: 2.0,
		Subj: func(u user.User) bool {
			return u.Gender == user.Male
		},
	}
	subj := user.User{Gender: user.Female}
	assert.Nil(
		t,
		trig.Match(&ents.Request{SubjUser: subj, Text: "abc"}),
	)
	subj.Gender = user.Male
	got := trig.Match(&ents.Request{SubjUser: subj, Text: "abc"})
	assert.Equal(t, got.Key, "key")
	assert.Equal(t, got.Subj, &subj)
}

func TestMatchMent1(t *testing.T) {
	trig := trigger.Trigger{
		Key:         "key",
		Probability: 2.0,
		Mentioned1: func(u user.User) bool {
			return u.Gender == user.Male
		},
	}
	subj := user.User{Gender: user.Female}
	ment := []user.User{{Gender: user.Female}}
	assert.Nil(
		t,
		trig.Match(&ents.Request{SubjUser: subj, MentionedUsers: ment, Text: "abc"}),
	)
	ment[0].Gender = user.Male
	got := trig.Match(&ents.Request{SubjUser: subj, MentionedUsers: ment, Text: "abc"})
	assert.Equal(t, got.Key, "key")
	assert.Equal(t, got.Mentioned, &ment[0])
}

func TestMatchMentN(t *testing.T) {
	trig := trigger.Trigger{
		Key:         "key",
		Probability: 2.0,
		MentionedN: func(u []user.User) bool {
			return len(u) == 1
		},
	}
	subj := user.User{Gender: user.Female}
	ment := []user.User{{}, {}}
	assert.Nil(
		t,
		trig.Match(&ents.Request{SubjUser: subj, MentionedUsers: ment, Text: "abc"}),
	)
	ment = []user.User{{}}
	got := trig.Match(&ents.Request{SubjUser: subj, MentionedUsers: ment, Text: "abc"})
	assert.Equal(t, got.Key, "key")
	assert.Nil(t, got.Mentioned)
}

func TestMatchRe(t *testing.T) {
	trig := trigger.Trigger{
		Key:         "key",
		Probability: 2.0,
		Re:          regexp.MustCompile("hi"),
	}
	subj := user.User{Gender: user.Female}
	assert.Nil(
		t,
		trig.Match(&ents.Request{SubjUser: subj, Text: "ok"}),
	)
	got := trig.Match(&ents.Request{SubjUser: subj, Text: "oh hi!"})
	assert.Equal(t, got.Key, "key")
}

func TestKarmaDiffComplex(t *testing.T) {
	trig := trigger.Trigger{
		Key:         "key",
		Probability: 2.0,
		KarmaDiff:   5,
	}
	subj := user.User{Gender: user.Female}
	got := trig.Match(&ents.Request{SubjUser: subj, Text: "ok"})
	assert.Equal(t, got.Key, "key")
	assert.Equal(t, got.KarmaDiff, 5)
}
