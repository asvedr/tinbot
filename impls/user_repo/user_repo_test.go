package user_repo_test

import (
	"testing"
	"tinbot/entities/user"
	"tinbot/impls/user_repo"
	"tinbot/proto"

	"github.com/stretchr/testify/assert"
)

const base_src = `{
	"by_id":{
		"1":{"Id":1,"Name":"user","Login":"@usr","Karma":-2,"Gender":1}
	},
	"login2id":{"@usr":1},
	"name2id":{"user":1}
}`

type IO struct {
	data  string
	write []string
}

type UserConfig struct{}

func (self *IO) ReadFile(path string) ([]byte, error) {
	return []byte(self.data), nil
}
func (self *IO) WriteFile(path string, data []byte) error {
	self.write = append(self.write, string(data))
	return nil
}
func (self *IO) CheckFileExists(path string) bool {
	return self.data != ""
}

func (UserConfig) ExtractNames(text string) []string {
	return nil
}

func (UserConfig) GetLogin(name string) string {
	return ""
}

func (UserConfig) GetName(login string) string {
	if login == "@usr" {
		return "alex"
	}
	return ""
}

func (UserConfig) GetGender(login string) user.Gender {
	if login == "@usr" {
		return user.Female
	}
	return user.Any
}

func dflt_user() user.User {
	return user.User{Id: 1, Name: "user", Login: "@usr", Karma: -2, Gender: 1}
}

func new_repo(data string, cnf proto.IUserConfig) (*IO, proto.IUserRepo) {
	io := &IO{data: data}
	repo, err := user_repo.New(io, "path", cnf)
	if err != nil {
		panic(err.Error())
	}
	return io, repo
}

func TestGetByIdFull(t *testing.T) {
	_, repo := new_repo(base_src, nil)
	usr, err := repo.GetById(1)
	assert.Nil(t, err)
	assert.Equal(t, dflt_user(), usr)
	_, err = repo.GetById(2)
	assert.Equal(t, err.Error(), "UserNotFound")
}

func TestGetByIdEmpty(t *testing.T) {
	_, repo := new_repo("", nil)
	_, err := repo.GetById(1)
	assert.Equal(t, err.Error(), "UserNotFound")
}

func TestGetByLogin(t *testing.T) {
	_, repo := new_repo(base_src, nil)

	got, err := repo.GetByLogin([]string{"@usr", "@notfound"})
	assert.Nil(t, err)
	assert.Equal(t, map[int64]user.User{1: dflt_user()}, got)

	got, err = repo.GetByLogin([]string{"@user", "user"})
	assert.Nil(t, err)
	assert.Equal(t, map[int64]user.User{}, got)
}

func TestGetByName(t *testing.T) {
	_, repo := new_repo(base_src, nil)

	got, err := repo.GetByName([]string{"user", "bebs"})
	assert.Nil(t, err)
	assert.Equal(t, map[int64]user.User{1: dflt_user()}, got)

	got, err = repo.GetByName([]string{"@usr", "usr"})
	assert.Nil(t, err)
	assert.Equal(t, map[int64]user.User{}, got)
}

func TestUpdateName(t *testing.T) {
	io, repo := new_repo(base_src, nil)
	assert.Equal(t, 0, len(io.write))

	usr := dflt_user()
	usr.Name = "ABC"
	err := repo.SetUsers(usr)
	assert.Equal(t, 1, len(io.write))
	assert.Nil(t, err)
	got, _ := repo.GetById(1)
	assert.Equal(t, usr, got)
}

func TestUpdateLogin(t *testing.T) {
	io, repo := new_repo(base_src, nil)
	assert.Equal(t, 0, len(io.write))

	usr := dflt_user()
	usr.Login = "ABC"
	err := repo.SetUsers(usr)
	assert.Equal(t, 1, len(io.write))
	assert.Nil(t, err)
	got, _ := repo.GetById(1)
	assert.Equal(t, usr, got)
}

func TestUpdateKarma(t *testing.T) {
	io, repo := new_repo(base_src, nil)
	assert.Equal(t, 0, len(io.write))

	usr := dflt_user()
	usr.Karma = 100
	err := repo.SetUsers(usr)
	assert.Equal(t, 1, len(io.write))
	assert.Nil(t, err)
	got, _ := repo.GetById(1)
	assert.Equal(t, usr, got)
}

func TestUpdateNone(t *testing.T) {
	io, repo := new_repo(base_src, nil)
	assert.Equal(t, 0, len(io.write))

	assert.Nil(t, repo.SetUsers(dflt_user()))
	assert.Equal(t, 0, len(io.write))
	usr, _ := repo.GetById(1)
	assert.Equal(t, dflt_user(), usr)
}

func TestCreateUser(t *testing.T) {
	io, repo := new_repo(base_src, nil)
	assert.Equal(t, 0, len(io.write))

	new_usr := dflt_user()
	new_usr.Id = 2
	assert.Nil(t, repo.SetUsers(new_usr))
	assert.Equal(t, 1, len(io.write))
	usr1, _ := repo.GetById(1)
	usr2, _ := repo.GetById(2)
	assert.Equal(t, usr1, dflt_user())
	usr1.Id = 2
	assert.Equal(t, usr1, usr2)
}

func TestClearKarma(t *testing.T) {
	io, repo := new_repo(base_src, nil)
	assert.Equal(t, 0, len(io.write))

	assert.Nil(t, repo.ClearKarma())
	assert.Equal(t, 1, len(io.write))

	usr, _ := repo.GetById(1)
	assert.Equal(t, usr.Karma, 0)

	assert.Nil(t, repo.ClearKarma())
	assert.Equal(t, 1, len(io.write))
}

func TestOverrideViaConfig(t *testing.T) {
	_, repo := new_repo("", UserConfig{})
	repo.SetUsers(dflt_user())
	usr, err := repo.GetById(dflt_user().Id)
	assert.Nil(t, err)
	assert.Equal(t, usr.Gender, user.Female)
	assert.Equal(t, usr.Name, "alex")
}
