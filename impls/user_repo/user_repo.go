package user_repo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"tinbot/entities/errs"
	"tinbot/entities/user"
	"tinbot/proto"
)

type repo struct {
	io     proto.IIO
	path   string
	state  state
	preset proto.IUserConfig
}

type state struct {
	ById      map[int64]user.User `json:"by_id"`
	LoginToId map[string]int64    `json:"login2id"`
	NameToId  map[string]int64    `json:"name2id"`
}

func New(
	io proto.IIO,
	path string,
	preset proto.IUserConfig,
) (proto.IUserRepo, error) {
	var state state
	if io.CheckFileExists(path) {
		bts, err := io.ReadFile(path)
		if err != nil {
			return nil, err
		}
		decoder := json.NewDecoder(bytes.NewBuffer(bts))
		decoder.DisallowUnknownFields()
		err = decoder.Decode(&state)
		if err != nil {
			return nil, fmt.Errorf("can not parse db: %v", err)
		}
	} else {
		state.ById = map[int64]user.User{}
		state.LoginToId = map[string]int64{}
		state.NameToId = map[string]int64{}
	}
	return &repo{
		path:   path,
		state:  state,
		io:     io,
		preset: preset,
	}, nil
}

func (self *repo) GetById(id int64) (user.User, error) {
	usr, found := self.state.ById[id]
	if !found {
		return usr, errs.UserNotFound{}
	}
	return usr, nil
}

func (self *repo) GetByLogin(logins []string) (map[int64]user.User, error) {
	ids := []int64{}
	for _, login := range logins {
		id, found := self.state.LoginToId[login]
		if found {
			ids = append(ids, id)
		}
	}
	return self.fetch_ids(ids), nil
}

func (self *repo) GetByName(names []string) (map[int64]user.User, error) {
	ids := []int64{}
	for _, name := range names {
		id, found := self.state.NameToId[name]
		if found {
			ids = append(ids, id)
		}
	}
	return self.fetch_ids(ids), nil
}

func (self *repo) SetUsers(usrs ...user.User) error {
	changed := false
	for _, usr := range usrs {
		self.modify_user_from_preset(&usr)
		in_db := self.state.ById[usr.Id]
		if reflect.DeepEqual(in_db, usr) {
			continue
		}
		self.state.ById[usr.Id] = usr
		self.upd_name_to_id(&in_db, &usr)
		self.upd_login_to_id(&in_db, &usr)
		changed = true
	}
	if !changed {
		return nil
	}
	bts, err := json.Marshal(self.state)
	if err != nil {
		return err
	}
	return self.io.WriteFile(self.path, bts)
}

func (self *repo) ClearKarma() error {
	upd := []user.User{}
	for _, usr := range self.state.ById {
		if usr.Karma != 0 {
			usr.Karma = 0
			upd = append(upd, usr)
		}
	}
	return self.SetUsers(upd...)
}

func (self *repo) fetch_ids(ids []int64) map[int64]user.User {
	res := map[int64]user.User{}
	for _, id := range ids {
		usr, found := self.state.ById[id]
		if !found {
			panic(fmt.Sprintf("inconsistent: id %v not found", id))
		}
		res[id] = usr
	}
	return res
}

func (self *repo) upd_name_to_id(in_db *user.User, usr *user.User) {
	if in_db.Name == usr.Name {
		return
	}
	delete(self.state.NameToId, in_db.Name)
	self.state.NameToId[usr.Name] = usr.Id
}

func (self *repo) upd_login_to_id(in_db *user.User, usr *user.User) {
	if in_db.Login == usr.Login {
		return
	}
	delete(self.state.LoginToId, in_db.Login)
	self.state.LoginToId[usr.Login] = usr.Id
}

func (self *repo) modify_user_from_preset(usr *user.User) {
	if self.preset == nil {
		return
	}
	name := self.preset.GetName(usr.Login)
	if name != "" {
		usr.Name = name
	}
	gender := self.preset.GetGender(usr.Login)
	if gender != user.Any {
		usr.Gender = gender
	}
}
