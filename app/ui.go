package app

import (
	"tinbot/impls/ui"
	"tinbot/proto"

	"github.com/asvedr/gotgbot/v2"
	"gitlab.com/asvedr/cldi/di"
)

var bot = di.Singleton[*gotgbot.Bot]{
	PureFunc: func() *gotgbot.Bot {
		token := Config.Get().Token
		return gotgbot.NewBotNoConnect(token, nil)
	},
}

var UI = di.Singleton[proto.IUI]{
	PureFunc: func() proto.IUI {
		return ui.New(bot.Get(), Engine.Get(), UserRepo.Get())
	},
}
