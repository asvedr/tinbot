package app

import (
	"tinbot/entrypoints/analyze_prob"
	"tinbot/entrypoints/config_help"
	"tinbot/entrypoints/find_unused"
	"tinbot/entrypoints/run_cli"
	"tinbot/entrypoints/run_tg"

	"gitlab.com/asvedr/cldi/cl"
	"gitlab.com/asvedr/cldi/di"
)

var EPRunTg = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return &run_tg.EP{
			Config: Config.Get,
			UI:     UI.Get,
		}
	},
}

var EPCli = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return &run_cli.EP{
			Users:  UserRepo.Get,
			Engine: Engine.Get,
		}
	},
}

var EPConfigHelp = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP { return config_help.EP{} },
}

var EPAnalyzeProbs = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return analyze_prob.EP{State: State.Get}
	},
}

var EPFundUnused = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return find_unused.EP{
			State:    State.Get,
			ReParser: ReParser.Get,
			Config:   Config.Get,
			Io:       IO.Get(),
		}
	},
}

var EPDebug = di.Factory[cl.IEP]{
	PureFunc: func() cl.IEP {
		return cl.NewRunnerMulty(
			"debug",
			"check functionality via CLI",
			EPCli.Get(),
			EPConfigHelp.Get(),
			EPAnalyzeProbs.Get(),
			EPFundUnused.Get(),
		)
	},
}
