package app

import (
	"tinbot/impls/engine"
	"tinbot/impls/user_repo"
	"tinbot/proto"

	"gitlab.com/asvedr/cldi/di"
)

var UserRepo = di.Singleton[proto.IUserRepo]{
	ErrFunc: func() (proto.IUserRepo, error) {
		return user_repo.New(
			IO.Get(),
			Config.Get().UserDbPath,
			State.Get().UserConfig,
		)
	},
}

var Engine = di.Singleton[proto.IEngine]{
	PureFunc: func() proto.IEngine {
		state := State.Get()
		return engine.New(
			UserRepo.Get(),
			state.UserConfig,
			state.Triggers,
			state.Reactions,
		)
	},
}
