package app

import (
	"tinbot/entities/config"
	"tinbot/impls/io"
	"tinbot/proto"

	"gitlab.com/asvedr/cldi/cl"
	"gitlab.com/asvedr/cldi/di"
)

var Config = di.Singleton[*config.Config]{
	ErrFunc: cl.BuildConfig[config.Config],
}

var IO = di.Singleton[proto.IIO]{PureFunc: io.New}
