package app

import (
	"tinbot/impls/reaction_parser"
	"tinbot/impls/reactor"
	"tinbot/impls/regex_parser"
	"tinbot/impls/state_maker"
	"tinbot/impls/trigger_parser"
	"tinbot/impls/user_config"
	"tinbot/proto"

	"gitlab.com/asvedr/cldi/di"
)

var react_parser = di.Singleton[proto.IReactionParser]{
	PureFunc: func() proto.IReactionParser {
		return reaction_parser.New(reactor.New)
	},
}

var ReParser = di.Singleton[proto.IRegexParser]{
	PureFunc: regex_parser.New,
}

var trig_parser = di.Singleton[proto.ITriggerParser]{
	PureFunc: trigger_parser.New,
}

var name_ex_parser = di.Singleton[proto.IUserConfigParser]{
	PureFunc: user_config.NewParser,
}

var StateMaker = di.Singleton[proto.IStateMaker]{
	PureFunc: func() proto.IStateMaker {
		return state_maker.New(
			IO.Get(),
			react_parser.Get(),
			ReParser.Get(),
			trig_parser.Get(),
			name_ex_parser.Get(),
		)
	},
}

var State = di.Singleton[proto.State]{
	ErrFunc: func() (proto.State, error) {
		return StateMaker.Get().Make(Config.Get())
	},
}
