package proto

import (
	"io"
	"regexp"
	"tinbot/entities/config"
)

type IReactionParser interface {
	Parse(io.Reader) (map[string]IReactor, error)
}

type IRegexParser interface {
	Parse(io.Reader) (map[string]*regexp.Regexp, error)
}

type ITriggerParser interface {
	Parse(map[string]*regexp.Regexp, io.Reader) ([]ITrigger, error)
}

type IUserConfigParser interface {
	Parse(io.Reader) (IUserConfig, error)
}

type State struct {
	Triggers   []ITrigger
	Reactions  map[string]IReactor
	UserConfig IUserConfig
}

type IStateMaker interface {
	Make(*config.Config) (State, error)
}
