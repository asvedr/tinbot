package proto

import (
	"tinbot/entities/trigger"
	"tinbot/entities/user"
)

type IIO interface {
	ReadFile(path string) ([]byte, error)
	WriteFile(path string, data []byte) error
	CheckFileExists(path string) bool
}

type IUserRepo interface {
	GetById(id int64) (user.User, error)
	GetByLogin(logins []string) (map[int64]user.User, error)
	GetByName(names []string) (map[int64]user.User, error)
	SetUsers(usrs ...user.User) error
	ClearKarma() error
}

type IUserConfig interface {
	ExtractNames(text string) []string
	GetLogin(name string) string
	GetName(login string) string
	GetGender(login string) user.Gender
}

type ITrigger interface {
	Schema() trigger.Schema
	Copy() ITrigger
	MultProb(float64)
	Match(*trigger.Request) *trigger.Triggered
}

type IReactor interface {
	React(
		subj *user.User,
		ment *user.User,
		vars map[string]string,
	) string
}

type IEngine interface {
	MultProb(mult float64)
	React(usr user.User, msg string) (string, error)
}

type IUI interface {
	Run()
}
