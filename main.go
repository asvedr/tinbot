package main

import (
	"tinbot/app"

	"gitlab.com/asvedr/cldi/cl"
)

var Version = "N/A"

func main() {
	cl.NewRunnerMulty(
		"",
		"robot oleg (version: "+Version+")",
		app.EPDebug.Get(),
		app.EPRunTg.Get(),
	).Run()
}
