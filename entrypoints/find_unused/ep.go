package find_unused

import (
	"bytes"
	"fmt"
	"tinbot/entities/config"
	"tinbot/proto"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct {
	State    func() proto.State
	ReParser func() proto.IRegexParser
	Config   func() *config.Config
	Io       proto.IIO
}

func (EP) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "unused",
		Description: "find unused regexps and reactions",
	}
}

func (self EP) Execute(_ string, _ any) error {
	config := self.Config()
	state := self.State()
	bts, err := self.Io.ReadFile(config.RegexPath)
	if err != nil {
		return err
	}
	regexes, err := self.ReParser().Parse(bytes.NewBuffer(bts))
	if err != nil {
		return err
	}
	trig_regs := map[string]bool{}
	trig_reacts := map[string]bool{}
	for _, trig := range state.Triggers {
		schema := trig.Schema()
		trig_regs[schema.ReName] = true
		trig_reacts[schema.Reaction] = true
	}
	found := false
	for react := range state.Reactions {
		if !trig_reacts[react] {
			fmt.Printf("unused reaction: '%s'\n", react)
			found = true
		}
	}
	for reg := range regexes {
		if !trig_regs[reg] {
			fmt.Printf("unused regex: '%s'\n", reg)
			found = true
		}
	}
	if !found {
		fmt.Printf("ok\n")
	}
	return nil
}
