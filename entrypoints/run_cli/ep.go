package run_cli

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strings"
	"tinbot/entities/user"
	"tinbot/proto"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct {
	Users  func() proto.IUserRepo
	Engine func() proto.IEngine
}

type request struct {
	Login    string  `short:"l" default:"user"`
	Gender   string  `short:"g" default:"m"`
	ProbMult float64 `short:"p" default:"1.0"`
}

func (*EP) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "cli",
		Description: "check bot via cli",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self *EP) Execute(prog string, args any) error {
	user_repo := self.Users()
	engine := self.Engine()
	req := args.(request)
	fmt.Printf(
		"login: %s\ngender: %v\nprob mult: %v\n",
		req.Login,
		req.Gender,
		req.ProbMult,
	)
	engine.MultProb(req.ProbMult)
	login := req.Login
	gender, conv := user.StringToGender(req.Gender)
	if !conv {
		return errors.New("invalid gender")
	}
	reader := bufio.NewReader(os.Stdin)
	usr, _ := user_repo.GetById(1)
	usr = user.User{
		Id:     1,
		Name:   login,
		Login:  "@" + login,
		Karma:  usr.Karma,
		Gender: gender,
	}
	err := user_repo.SetUsers(usr)
	if err != nil {
		return err
	}
	for {
		fmt.Printf("%s> ", login)
		line, err := reader.ReadString('\n')
		if err != nil {
			return err
		}
		line = strings.TrimSpace(line)
		usr, err := user_repo.GetById(usr.Id)
		if err != nil {
			return err
		}
		resp, err := engine.React(usr, line)
		if err != nil {
			return err
		}
		if resp == "" {
			fmt.Printf("<none>\n")
		} else {
			fmt.Printf("tinbot> %s\n", resp)
		}
	}
}
