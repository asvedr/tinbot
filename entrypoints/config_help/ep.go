package config_help

import (
	"tinbot/entities/config"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct{}

func (EP) Schema() cl.EPSchema {
	return cl.EPSchema{Command: "config", Description: "get config schema"}
}

func (self EP) Execute(prog string, args any) error {
	println(cl.GetConfigHelp[config.Config]())
	return nil
}
