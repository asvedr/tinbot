package run_tg

import (
	"errors"
	"tinbot/entities/config"
	"tinbot/proto"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct {
	Config func() *config.Config
	UI     func() proto.IUI
}

func (*EP) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "tg",
		Description: "run bot via tg",
	}
}

func (self *EP) Execute(prog string, args any) error {
	if self.Config().Token == "" {
		return errors.New("Telegram token not set")
	}
	self.UI().Run()
	return nil
}
