package analyze_prob

import (
	"errors"
	"fmt"
	"math/rand"
	"sort"
	"tinbot/proto"

	"gitlab.com/asvedr/cldi/cl"
	"gitlab.com/asvedr/tablemaker"
)

type EP struct {
	State func() proto.State
}

type request struct {
	RegexName string `short:"r" description:"by specific regex" default:""`
	ReactName string `short:"R" description:"by specific reaction" default:""`
	Mode      string `short:"m" default:"regex" description:"'regex' or 'react'"`
	HideIfGt  int    `short:"g" default:"100" description:"hide row if prob > value"`
}

func (EP) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "probs",
		Description: "reveal probabilities for each RE",
		ParamParser: cl.MakeParser[request](),
	}
}

func (self EP) Execute(prog string, args any) error {
	triggers := self.State().Triggers
	req := args.(request)
	if req.RegexName != "" {
		return make_one(group_by_regex(triggers), req.RegexName)
	}
	if req.ReactName != "" {
		return make_one(group_by_react(triggers), req.ReactName)
	}
	switch req.Mode {
	case "regex":
		return make_total(group_by_regex(triggers), "regex", req.HideIfGt)
	case "react":
		return make_total(group_by_react(triggers), "react", req.HideIfGt)
	default:
		return errors.New("invalid mode")
	}
}

func make_total(
	triggers map[string][]proto.ITrigger,
	key string,
	hide_if_gt int,
) error {
	maker := tablemaker.New()
	maker.SetHeader(key, "prob (count)")
	maker.SetHeaderBodySeparator('-')
	maker.SetColumnSeparator(" | ")
	for _, re := range sorted_keys(triggers) {
		filtered := triggers[re]
		sum_prob := get_total_prob(filtered)
		if int(sum_prob*100) > hide_if_gt {
			continue
		}
		l := len(filtered)
		val := fmt.Sprintf("%v (%d)", as_prob(sum_prob), l)
		maker.AddRow(re, val)
	}
	tbl, err := maker.Render()
	println(tbl)
	return err
}

func make_one(
	triggers map[string][]proto.ITrigger,
	key string,
) error {
	filtered, found := triggers[key]
	if !found {
		return fmt.Errorf("no triggers for key: '%s'", key)
	}
	sum_prob := get_total_prob(filtered)
	fmt.Printf("sum prob: %v\n", as_prob(sum_prob))
	maker := tablemaker.New()
	maker.SetHeader("regex", "react", "prob")
	maker.SetColumnSeparator(" | ")
	maker.SetHeaderBodySeparator('-')
	for _, trigger := range filtered {
		schema := trigger.Schema()
		maker.AddRow(schema.ReName, schema.Reaction, schema.Prob)
	}
	tbl, err := maker.Render()
	println(tbl)
	return err
}

func get_total_prob(triggers []proto.ITrigger) float64 {
	var probs []float64
	for _, trigger := range triggers {
		probs = append(probs, trigger.Schema().Prob)
	}
	total := 20000
	matched := 0
	for i := 0; i < total; i += 1 {
		if try(probs) {
			matched += 1
		}
	}
	return float64(matched) / float64(total)
}

func try(probs []float64) bool {
	for _, threshold := range probs {
		if rand.Float64() < threshold {
			return true
		}
	}
	return false
}

func sorted_keys[T any](src map[string]T) []string {
	res := []string{}
	for key := range src {
		res = append(res, key)
	}
	ss := sort.StringSlice(res)
	ss.Sort()
	return []string(ss)
}

func as_prob(prob float64) string {
	return fmt.Sprintf("%d%%", int(prob*100))
}

func group_by_regex(triggers []proto.ITrigger) map[string][]proto.ITrigger {
	res := map[string][]proto.ITrigger{}
	for _, trigger := range triggers {
		schema := trigger.Schema()
		re := schema.ReName
		res[re] = append(res[re], trigger)
	}
	return res
}

func group_by_react(triggers []proto.ITrigger) map[string][]proto.ITrigger {
	res := map[string][]proto.ITrigger{}
	for _, trigger := range triggers {
		schema := trigger.Schema()
		re := schema.Reaction
		res[re] = append(res[re], trigger)
	}
	return res
}
