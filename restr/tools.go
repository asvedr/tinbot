package restr

import "strings"

func JoinOr(vals ...string) string {
	var res string
	for i, val := range vals {
		if i == 0 {
			res = "(" + val + ")"
		} else {
			res += "|(" + val + ")"
		}
	}
	return res
}

func JoinSeq(vals ...string) string {
	return strings.Join(vals, ".*")
}
