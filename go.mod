module tinbot

go 1.21.3

require (
	github.com/asvedr/gotgbot/v2 v2.0.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	gitlab.com/asvedr/cldi v0.6.0 // indirect
	gitlab.com/asvedr/tablemaker v0.0.0-20240115125530-e0eabb49d12a // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
