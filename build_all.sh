#!/bin/sh
export VERSION="$(cat version.txt)"
export LD_FLAGS="-ldflags='-X main.Version=$VERSION'"
set -e
echo "Version: $VERSION"

echo "Build robot"
CMD="go build $LD_FLAGS"
echo "$CMD"
sh -c "$CMD"

